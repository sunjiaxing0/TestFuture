%% GetSet is a custom storage class that is used to define data whose
%% values are read and written to using "get" and "set" functions.

%% Copyright 1990-2015 The MathWorks, Inc.

%implements * "C"

%% Function: FcnPreProcessIndex ==============================================
%% Abstract:
%%   Validate and pre-process index for GetSet custom storage class
%%
%function FcnPreProcessIndex(idx) void
  %if ISEMPTY(idx)
    %% Scalar data
    %return ""
  %endif
  
  %assign length = SIZE(idx, 1)
  
  %if ((idx[0] == "[") && (idx[length-1] == "]"))
    %% Get rid of the [] brackets
    %assign idx[0] = " "
    %assign idx[length-1] = " "
  %else
    %assign errTxt = "Invalid index '%<idx>' for GetSet custom storage class."
    %<LibReportFatalError(errTxt)>
  %endif

  %return idx
%endfunction

%% Function: FcnReplaceIdentifierToken =======================================
%% Abstract:
%%   Replace identifier token in function name
%function FcnReplaceIdentifierToken(fcnName, identifier)
  %assign fcnName = FEVAL("strrep", fcnName, "$N", identifier)
  %return fcnName
%endfunction
  
%% Function: DataAccess ======================================================
%% Abstract:
%%   DataAccess provides an API for requesting code fragments or other
%%   information corresponding to data of this custom storage class.
%%
%function DataAccess(record, request, idx, reim, extra) void

  %switch request
 
    %case "initialize"
      %assign props  = LibGetCustomStorageAttributes(record)
      %if ISFIELD(props, "SetString")
        %return LibDefaultCustomStorageInitialize(record, idx, reim)	  
      %else
        %return ""
      %endif
      %%break
      
    %case "contents"
      %assign props  = LibGetCustomStorageAttributes(record)
      %if ISFIELD(props, "GetString")
        %assign getFcn = props.GetString
        %return "%<getFcn>"
      %else
        %% Use the default reference to a global variable.
        %return LibDefaultCustomStorageContents(record, idx, reim)
      %endif
      %%break

    %case "address"
      %assign props  = LibGetCustomStorageAttributes(record)
      %if ISFIELD(props, "SetString")
        %return LibDefaultCustomStorageAddress(record, idx, reim)
      %else
        %% Use the default address of a global variable.
        %return "address not supported for accessors"
      %endif
      %%break

    %case "declare"
      %assign props  = LibGetCustomStorageAttributes(record)
      %if ISFIELD(props, "SetString")
        %return "extern %<LibDefaultCustomStorageDeclare(record)>"
      %else
        %% Get functions must be declared in external header file.
        %% Users can specify the HeaderFile on the data object or include it
        %% in the custom code section of the model's configuration parameters.
        %return ""
      %endif
      %%break

    %case "define"
      %assign props  = LibGetCustomStorageAttributes(record)
      %if ISFIELD(props, "SetString")
        %return "%<LibDefaultCustomStorageDefine(record)>"
      %else
        %% Get functions must be defined by external code.
        %return ""
      %endif
      %%break
      
    %case "layout"
      %% Use default data layout.
      %return LibDefaultCustomStorageLayout(record)
      %%break

    %case "qualifier"
      %% No type qualifier.
      %return ""
      %%break
      
    %case "set"
      %assign props  = LibGetCustomStorageAttributes(record)
      %assign setFcn = props.SetString
      %assign name = record.Identifier
      
      %return "%<name> = %<extra>;\n%<setFcn>;\n"

    %default
      %% Unknown access type.
      %return LibDefaultCustomStorageUnknownDataAccessType ...
    (record, request, idx, reim)
      %%break

  %endswitch

%endfunction



%% Function: ClassAccess ============================================
%% Abstract:
%%   ClassAccess provides an API for requesting information or action
%%   corresponding to the custom storage class
%%
%function ClassAccess(record, request) void

  %switch request
    %case "setup"
      %assign numData = LibCustomStorageClassRecordNumData(record)
      %foreach idx = numData

        %% Get the idx'th data record
        %assign item = LibGetDataRecord(record, idx)

        %% Make sure the record is non-complex.
        %<LibCustomStorageVerifyRecordIsNonComplex(item)>

      %endforeach
      %return
      %%break

    %case "comment"
      %return LibDefaultCustomStorageComment(record)
      %%break

    %default
      %% Unknown access type.
      %return LibDefaultCustomStorageUnknownClassAccessType ...
    (record, request)
      %%break
      
  %endswitch
      
%endfunction


%% Function: Version ============================================
%% Abstract:
%%
%function Version(record) void
  %return 2
%endfunction
